package com.xm.exercise.repository;

import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
public class TradeRepositoryTest {

    @Autowired
    private TradeRepository repository;

    @Test
    public void save() {
        Trade trade = new Trade();
        trade.setUid(UUID.randomUUID().toString());
        trade.setStatus(TradeStatus.PENDING_EXECUTION);
        trade.setPrice(new BigDecimal("1.123"));
        trade.setCreateDate(new Date());
        trade.setSide(TradeSide.BUY);
        trade.setSymbol("EUR/USD");
        trade.setQuantity(1000L);

        Trade beforeSave = repository.findFirstByUid(trade.getUid());
        Assertions.assertNull(beforeSave);

        Assertions.assertNull(trade.getId());
        repository.save(trade);
        Assertions.assertNotNull(trade.getId());

        Trade afterSave = repository.findFirstByUid(trade.getUid());
        Assertions.assertEquals(trade, afterSave);
    }

}
