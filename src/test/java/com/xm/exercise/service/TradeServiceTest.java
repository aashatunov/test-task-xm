package com.xm.exercise.service;

import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import com.xm.exercise.validation.SymbolValidator;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@SpringBootTest
public class TradeServiceTest {

    @Autowired
    private TradeService tradeService;

    @Autowired
    private MessageSource messageSource;

    @Test
    public void whenSymbolInvalid_thenThrowException() {
        try {
            tradeService.create(TradeSide.BUY, null, 100L, new BigDecimal("1.22"));
        } catch (ConstraintViolationException e) {
            assertEquals("trade.symbol.invalid", e);
        }
    }

    @Test
    public void whenSymbolValid_thenSave() {
        tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("1.22"));
        Trade trade = tradeService.create(TradeSide.BUY, SymbolValidator.USD_JPY, 100L, new BigDecimal("1.22"));
        Assertions.assertEquals(TradeStatus.PENDING_EXECUTION, trade.getStatus());
    }

    @Test
    public void whenPriceInvalid_thenThrowException() {
        try {
            tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("0"));
        } catch (ConstraintViolationException e) {
            assertEquals("trade.price.invalid", e);
        }
        try {
            tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("-1"));
        } catch (ConstraintViolationException e) {
            assertEquals("trade.price.invalid", e);
        }
    }

    @Test
    public void whenPriceValid_thenSave() throws Exception {
        tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("0.001"));
        tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("111111"));

        Trade trade = tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 100L, new BigDecimal("0.00001"));
        Assertions.assertEquals(trade.getPrice(), new BigDecimal("0.00001"));

        trade = tradeService.getByUid(trade.getUid());
        Assertions.assertEquals(new BigDecimal("0.00001"), trade.getPrice());
    }

    @Test
    public void whenQuantityInvalid_thenThrowException() {
        try {
            tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 0L, new BigDecimal("10"));
        } catch (ConstraintViolationException e) {
            assertEquals("trade.quantity.invalid", e);
        }
        try {
            tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1_000_001L, new BigDecimal("10"));
        } catch (ConstraintViolationException e) {
            assertEquals("trade.quantity.invalid", e);
        }
    }

    @Test
    public void whenQuantityValid_thenSave() {
        tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1L, new BigDecimal("10"));
        tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1_000_000L, new BigDecimal("10"));
    }

    @Test
    public void whenCreated_thenCanBeUpdated() {
        Trade trade = tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1L, new BigDecimal("1"));

        Trade updated1 = tradeService.update(trade.getUid(), TradeStatus.EXECUTED, null);
        Assertions.assertNotEquals(trade.getStatus(), updated1.getStatus());
        Assertions.assertEquals(TradeStatus.EXECUTED, updated1.getStatus());
        Assertions.assertNotNull(updated1.getUpdateDate());

        Trade updated2 = tradeService.update(trade.getUid(), TradeStatus.EXECUTED, null);
        Assertions.assertTrue(updated2.getUpdateDate().after(updated1.getUpdateDate()));
    }

    @Test
    public void whenCreated_thenCanBeFound() {
        Trade trade = tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1L, new BigDecimal("1"));

        Trade created = tradeService.getByUid(trade.getUid());
        Assertions.assertEquals(trade.getStatus(), created.getStatus());
        Assertions.assertEquals(trade, created);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void whenGetAll_thenReturnBasedOnPagingParams() {
        int count = 10;
        for (int i = 0; i < count; i++) {
            tradeService.create(TradeSide.BUY, SymbolValidator.EUR_USD, 1L, new BigDecimal("1"));
        }

        List<Trade> all;

        // offset = null, limit = null
        all = tradeService.getAll(null, null);
        Assertions.assertEquals(count, all.size());

        // offset = null, limit = 0
        all = tradeService.getAll(null, 0);
        Assertions.assertEquals(count, all.size());

        // offset = 0, limit = 4
        int limit = 4;
        all = tradeService.getAll(0, limit);
        Assertions.assertEquals(limit, all.size());
        // offset = 4, limit = 4
        all = tradeService.getAll(limit, limit);
        Assertions.assertEquals(limit, all.size());
        // offset = 8, limit = 4
        all = tradeService.getAll(limit * 2, limit);
        Assertions.assertEquals(2, all.size());

        // offset = 0, limit > count
        all = tradeService.getAll(0, count + 1);
        Assertions.assertEquals(count, all.size());
    }

    private void assertEquals(String expected, ConstraintViolationException e) {
        Assertions.assertEquals(messageSource.getMessage(expected, null, Locale.getDefault()), e.getConstraintViolations().stream().findFirst().map(ConstraintViolation::getMessage).orElseThrow());
    }

}
