package com.xm.exercise.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xm.exercise.controller.data.TradeData;
import com.xm.exercise.controller.data.TradeRequest;
import com.xm.exercise.controller.data.TradeStatusData;
import com.xm.exercise.controller.mapper.TradeMapper;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest
@AutoConfigureMockMvc
public class TradeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TradeMapper tradeMapper;
    @Autowired
    private MessageSource messageSource;

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Scenario - Submit Sell trade request with successful execution.
     */
    @Test
    public void submitSellRequestWithSuccess() throws Exception {
        submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid symbols must be rejected with a 400 - Bad Request http code and along with a
     * validation error message i.e. 'Symbol valid values: USD/JPY, EUR/USD'.
     */
    @Test
    public void submitSellRequestWithInvalidSymbols() throws Exception {
        submitTradeRequestWithFail("/api/sell", getMessage("trade.symbol.invalid"), """
                {
                    "symbol": "EUR/EUR",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid values must be rejected with a 400 - Bad Request http code along with a validation
     * error message i.e. 'quantity must be greater than 0 and less than or equal to 1M'.
     */
    @Test
    public void submitSellRequestWithInvalidQuantity() throws Exception {
        submitTradeRequestWithFail("/api/sell", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 0,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithFail("/api/sell", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": -1,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithFail("/api/sell", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000001,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1000000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid values must be rejected with a 400 - Bad Request http code and proper error message
     * i.e. 'price must be greater than 0'.
     */
    @Test
    public void submitSellRequestWithInvalidPrice() throws Exception {
        submitTradeRequestWithFail("/api/sell", getMessage("trade.price.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": -1
                }
                """);
        submitTradeRequestWithFail("/api/sell", getMessage("trade.price.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 0
                }
                """);
    }

    /**
     * Scenario - Submit Buy trade request with successful execution.
     */
    @Test
    public void submitBuyRequestWithSuccess() throws Exception {
        submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid symbols must be rejected with a 400 - Bad Request http code and along with a
     * validation error message i.e. 'Symbol valid values: USD/JPY, EUR/USD'.
     */
    @Test
    public void submitBuyRequestWithInvalidSymbols() throws Exception {
        submitTradeRequestWithFail("/api/buy", getMessage("trade.symbol.invalid"), """
                {
                    "symbol": "EUR/EUR",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid values must be rejected with a 400 - Bad Request http code along with a validation
     * error message i.e. 'quantity must be greater than 0 and less than or equal to 1M'.
     */
    @Test
    public void submitBuyRequestWithInvalidQuantity() throws Exception {
        submitTradeRequestWithFail("/api/buy", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 0,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithFail("/api/buy", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": -1,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithFail("/api/buy", getMessage("trade.quantity.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000001,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1,
                    "price": 1.123
                }
                """);
        submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "USD/JPY",
                    "quantity": 1000000,
                    "price": 1.123
                }
                """);
    }

    /**
     * Scenario - Invalid values must be rejected with a 400 - Bad Request http code and proper error message
     * i.e. 'price must be greater than 0'.
     */
    @Test
    public void submitBuyRequestWithInvalidPrice() throws Exception {
        submitTradeRequestWithFail("/api/buy", getMessage("trade.price.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": -100
                }
                """);
        submitTradeRequestWithFail("/api/buy", getMessage("trade.price.invalid"), """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 0
                }
                """);
    }

    /**
     * Scenario - Get trade status.
     */
    @Test
    public void getTradeStatus() throws Exception {
        TradeData trade = submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);

        String statusJson = performGet("/api/trades/" + trade.getId() + "/status")
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        TradeStatusData statusData = fromJson(statusJson, TradeStatusData.class);
        Assertions.assertNotNull(statusData.getStatus());
    }

    /**
     * Scenario - Get trade details.
     */
    @Test
    public void getTradeDetails() throws Exception {
        TradeData trade = submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);

        String tradeJson = performGet("/api/trades/" + trade.getId())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        TradeData tradeData = fromJson(tradeJson, TradeData.class);
        Assertions.assertEquals(trade.getId(), tradeData.getId());
        Assertions.assertEquals(TradeSide.SELL, tradeData.getSide());
        Assertions.assertEquals(trade.getSide(), tradeData.getSide());
        Assertions.assertEquals(trade.getPrice(), tradeData.getPrice());
        Assertions.assertEquals(trade.getQuantity(), tradeData.getQuantity());
        Assertions.assertNotNull(tradeData.getTimestamp());
    }

    /**
     * Scenario - Get all trades.
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void getAllTrades() throws Exception {
        TypeReference<List<TradeData>> typeReference = new TypeReference<>() {
        };

        String tradesJson = performGet("/api/trades")
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<TradeData> trades = fromJson(tradesJson, typeReference);
        Assertions.assertEquals(0, trades.size());

        TradeData trade1 = submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);
        TradeData trade2 = submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 10,
                    "price": 1.456
                }
                """);

        tradesJson = performGet("/api/trades")
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        trades = fromJson(tradesJson, typeReference);
        Assertions.assertEquals(2, trades.size());
        Assertions.assertEquals(trade1.getId(), trades.get(0).getId());
        Assertions.assertEquals(trade2.getId(), trades.get(1).getId());
    }

    /**
     * Scenario - If no response is received within 1 minute
     * the trade can be considered as NOT_EXECUTED with reason 'trade expired'.
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void getTradeDetailsAfterBrokerTimeout() throws Exception {
        ReflectionTestUtils.setField(tradeMapper, "brokerTimeout", 0);

        TradeData trade = submitTradeRequestWithSuccess("/api/sell", TradeSide.SELL, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);

        String tradeJson = performGet("/api/trades/" + trade.getId())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        TradeData tradeData = fromJson(tradeJson, TradeData.class);

        Assertions.assertEquals(TradeStatus.NOT_EXECUTED, tradeData.getStatus());
        Assertions.assertEquals(getMessage("trade.reason.expired"), tradeData.getReason());
    }

    /**
     * Scenario - broker executed a trade.
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    public void getTradeDetailsAfterBrokerExecution() throws Exception {
        TradeData trade = submitTradeRequestWithSuccess("/api/buy", TradeSide.BUY, """
                {
                    "symbol": "EUR/USD",
                    "quantity": 1000,
                    "price": 1.123
                }
                """);

        TradeData tradeData = null;
        for (int i = 0; i < 120; i++) {
            String tradeJson = performGet("/api/trades/" + trade.getId())
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            tradeData = fromJson(tradeJson, TradeData.class);
            if (tradeData.getStatus() != TradeStatus.PENDING_EXECUTION) {
                break;
            } else {
                Thread.sleep(500);
            }
        }
        Assertions.assertNotEquals(TradeStatus.PENDING_EXECUTION, tradeData.getStatus());
    }

    private TradeData submitTradeRequestWithSuccess(String path, TradeSide side, String bodyJson) throws Exception {
        // submit request
        String location = performPost(path, bodyJson)
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn()
                .getResponse()
                .getHeader(HttpHeaders.LOCATION);
        Assertions.assertNotNull(location);

        // extract uid from Location header
        Pattern pattern = Pattern.compile(".*/api/trades/(.*)");
        Matcher matcher = pattern.matcher(location);
        Assertions.assertTrue(matcher.find());
        String uid = matcher.group(1);

        // check trade exists in the given location
        String result1 = performGet("/api/trades/" + uid)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        TradeData trade1 = fromJson(result1, TradeData.class);

        String result2 = performGet(location)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        TradeData trade2 = fromJson(result2, TradeData.class);

        Assertions.assertEquals(trade1.getSide(), trade2.getSide());
        Assertions.assertEquals(trade1.getPrice(), trade2.getPrice());
        Assertions.assertEquals(trade1.getSymbol(), trade2.getSymbol());
        Assertions.assertEquals(trade1.getQuantity(), trade2.getQuantity());

        // check response contains all fields from the request
        TradeRequest request = fromJson(bodyJson, TradeRequest.class);
        Assertions.assertEquals(side, trade1.getSide());
        Assertions.assertEquals(request.getPrice(), trade1.getPrice());
        Assertions.assertEquals(request.getSymbol(), trade1.getSymbol());
        Assertions.assertEquals(request.getQuantity(), trade1.getQuantity());

        return trade2;
    }

    private void submitTradeRequestWithFail(String path, String errorMessage, String bodyJson) throws Exception {
        String response = performPost(path, bodyJson)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assertions.assertEquals(errorMessage, response);
    }

    private ResultActions performPost(String path, String bodyJson) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                        .post(path)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(bodyJson)
                )
                .andDo(MockMvcResultHandlers.print());
    }

    private ResultActions performGet(String path, Object... params) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                        .get(path, params)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(MockMvcResultHandlers.print());
    }

    private <T> T fromJson(String json, Class<T> clazz) throws Exception {
        return mapper.readValue(json, clazz);
    }

    private <T> T fromJson(String json, TypeReference<T> type) throws Exception {
        return mapper.readValue(json, type);
    }

    private String getMessage(String key) {
        return messageSource.getMessage(key, null, Locale.getDefault());
    }

}
