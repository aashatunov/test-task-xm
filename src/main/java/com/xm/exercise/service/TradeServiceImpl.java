package com.xm.exercise.service;

import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import com.xm.exercise.repository.TradeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@Validated
public class TradeServiceImpl implements TradeService {

    private final BrokerService brokerService;
    private final TradeRepository tradeRepository;

    @Autowired
    public TradeServiceImpl(BrokerService brokerService, TradeRepository tradeRepository) {
        this.brokerService = brokerService;
        this.tradeRepository = tradeRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Trade create(TradeSide side, String symbol, Long quantity, BigDecimal price) {
        // create entity
        Trade trade = new Trade();
        trade.setUid(UUID.randomUUID().toString());
        trade.setStatus(TradeStatus.PENDING_EXECUTION);
        trade.setCreateDate(new Date());
        trade.setQuantity(quantity);
        trade.setSymbol(symbol);
        trade.setPrice(price);
        trade.setSide(side);

        log.debug("create - request :: {}", trade);

        // persist entity
        tradeRepository.save(trade);

        log.debug("create - response :: id={}", trade.getId());

        // notify broker
        brokerService.save(trade);

        return trade;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Trade update(String uuid, TradeStatus status, String reason) {
        log.debug("update - request :: uuid={}, status={}, reason={}", uuid, status, reason);
        Trade trade = tradeRepository.findFirstByUid(uuid);
        if (trade != null) {
            log.debug("update - response :: {}", trade);
            trade.setUpdateDate(new Date());
            trade.setStatus(status);
            trade.setReason(reason);
            tradeRepository.save(trade);
        } else {
            log.error("trade not found :: uuid={}", uuid);
        }
        return trade;
    }

    @Override
    public List<Trade> getAll(Integer offset, Integer limit) {
        Pageable pageable;
        if (limit == null || limit <= 0) {
            pageable = Pageable.unpaged();
        } else {
            pageable = Pageable.ofSize(limit);
            if (offset != null && offset > 0 && offset % limit == 0) {
                pageable = pageable.withPage(offset / limit);
            }
        }
        log.debug("getAll - request :: offset={}, limit={}, page={}", offset, limit, pageable);
        Page<Trade> page = tradeRepository.findAll(pageable);
        log.debug("getAll - response :: totalElements={}, totalPages={}", page.getTotalElements(), page.getTotalPages());
        return page.getContent();
    }

    @Override
    public Trade getByUid(String uid) {
        log.debug("getByUid - request :: {}", uid);
        Trade trade = tradeRepository.findFirstByUid(uid);
        log.debug("getByUid - response :: {}", trade);
        return trade;
    }
}

