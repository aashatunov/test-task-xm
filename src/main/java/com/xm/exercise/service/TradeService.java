package com.xm.exercise.service;

import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import com.xm.exercise.validation.Price;
import com.xm.exercise.validation.Quantity;
import com.xm.exercise.validation.Symbol;
import jakarta.validation.Valid;

import java.math.BigDecimal;
import java.util.List;

public interface TradeService {

    Trade create(TradeSide side, @Valid @Symbol String symbol, @Valid @Quantity Long quantity, @Valid @Price BigDecimal price);

    Trade update(String uuid, TradeStatus status, String reason);

    List<Trade> getAll(Integer offset, Integer limit);

    Trade getByUid(String uid);

}
