package com.xm.exercise.service;

import com.broker.external.BrokerResponseCallback;
import com.xm.exercise.domain.TradeStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
class BrokerServiceCallbackHandler implements BrokerResponseCallback {

    private final TradeService tradeService;

    @Autowired
    public BrokerServiceCallbackHandler(@Lazy TradeService tradeService) {
        this.tradeService = tradeService;
    }

    @Override
    public void successful(UUID tradeId) {
        tradeService.update(tradeId.toString(), TradeStatus.EXECUTED, null);
    }

    @Override
    public void unsuccessful(UUID tradeId, String reason) {
        tradeService.update(tradeId.toString(), TradeStatus.NOT_EXECUTED, reason);
    }

}
