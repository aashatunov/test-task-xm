package com.xm.exercise.service;

import com.broker.external.BrokerResponseCallback;
import com.broker.external.BrokerTrade;
import com.broker.external.BrokerTradeSide;
import com.broker.external.ExternalBroker;
import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
class BrokerServiceImpl implements BrokerService {

    private final ExternalBroker broker;

    @Autowired
    BrokerServiceImpl(BrokerResponseCallback brokerCallback) {
        this.broker = new ExternalBroker(brokerCallback);
    }

    @Override
    public void save(Trade trade) {
        BrokerTrade brokerTrade = new BrokerTrade(
                UUID.fromString(trade.getUid()),
                trade.getSymbol(),
                trade.getQuantity(),
                map(trade.getSide()),
                trade.getPrice()
        );
        broker.execute(brokerTrade);
    }

    private BrokerTradeSide map(TradeSide from) {
        return switch (from) {
            case BUY -> BrokerTradeSide.BUY;
            case SELL -> BrokerTradeSide.SELL;
        };
    }
}
