package com.xm.exercise.service;

import com.xm.exercise.domain.Trade;

public interface BrokerService {

    void save(Trade trade);

}
