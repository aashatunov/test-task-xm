package com.xm.exercise.repository;

import com.xm.exercise.domain.Trade;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;

public interface TradeRepository extends ListCrudRepository<Trade, Long>, ListPagingAndSortingRepository<Trade, Long> {

    Trade findFirstByUid(String uid);

}
