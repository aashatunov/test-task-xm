package com.xm.exercise.controller.mapper;

import com.xm.exercise.controller.data.TradeData;
import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class TradeMapper {

    private final Integer brokerTimeout;
    private final MessageSource messageSource;

    public TradeMapper(@Value("${app.brokerTimeoutInSeconds}") Integer brokerTimeout, MessageSource messageSource) {
        this.brokerTimeout = brokerTimeout;
        this.messageSource = messageSource;
    }

    public TradeData from(Trade trade) {
        TradeData data = new TradeData();
        data.setId(trade.getUid());
        data.setQuantity(trade.getQuantity());
        data.setSymbol(trade.getSymbol());
        data.setSide(trade.getSide());
        data.setStatus(trade.getStatus());
        data.setReason(trade.getReason());
        data.setTimestamp(trade.getCreateDate());
        data.setPrice(trade.getPrice());
        if (data.getStatus() == TradeStatus.PENDING_EXECUTION) {
            long diff = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - data.getTimestamp().getTime());
            if (diff >= brokerTimeout) {
                log.warn("trade expired :: trade={}", trade);
                data.setReason(messageSource.getMessage("trade.reason.expired", null, Locale.getDefault()));
                data.setStatus(TradeStatus.NOT_EXECUTED);
            }
        }
        return data;
    }

    public List<TradeData> fromList(List<Trade> trades) {
        return trades.stream().map(this::from).toList();
    }

}
