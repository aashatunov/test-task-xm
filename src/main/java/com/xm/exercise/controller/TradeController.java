package com.xm.exercise.controller;

import com.xm.exercise.controller.data.TradeData;
import com.xm.exercise.controller.data.TradeRequest;
import com.xm.exercise.controller.data.TradeStatusData;
import com.xm.exercise.controller.mapper.TradeMapper;
import com.xm.exercise.domain.Trade;
import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.service.TradeService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class TradeController {

    private final TradeMapper tradeMapper;
    private final TradeService tradeService;

    public TradeController(TradeMapper tradeMapper, TradeService tradeService) {
        this.tradeMapper = tradeMapper;
        this.tradeService = tradeService;
    }

    @PostMapping(value = "/sell", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    void sell(@RequestBody TradeRequest request, HttpServletResponse response) {
        buySell(TradeSide.SELL, request, response);
    }

    @PostMapping(value = "/buy", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    void buy(@RequestBody TradeRequest request, HttpServletResponse response) {
        buySell(TradeSide.BUY, request, response);
    }

    @GetMapping(value = "/trades/{tradeId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    TradeData getByUid(@PathVariable("tradeId") String tradeId) {
        log.debug("getByUid - request :: tradeId={}", tradeId);
        Trade trade = tradeService.getByUid(tradeId);
        log.debug("getByUid - response :: trade={}", trade);
        return tradeMapper.from(trade);
    }

    @GetMapping(value = "/trades/{tradeId}/status", produces = {MediaType.APPLICATION_JSON_VALUE})
    TradeStatusData getStatus(@PathVariable("tradeId") String tradeId) {
        log.debug("getStatus - request :: tradeId={}", tradeId);
        Trade trade = tradeService.getByUid(tradeId);
        TradeData tradeData = tradeMapper.from(trade);
        log.debug("getStatus - response :: trade={}", trade);
        TradeStatusData data = new TradeStatusData();
        data.setStatus(tradeData.getStatus());
        return data;
    }

    @GetMapping(value = "/trades", produces = {MediaType.APPLICATION_JSON_VALUE})
    List<TradeData> getAll(@RequestParam(value = "offset", required = false) Integer offset, @RequestParam(value = "limit", required = false) Integer limit) {
        log.debug("getAll - request :: offset={}, limit={}", offset, limit);
        List<Trade> trades = tradeService.getAll(offset, limit);
        log.debug("getAll - response :: trades={}", trades.size());
        return tradeMapper.fromList(trades);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    ResponseEntity<String> onValidationException(ConstraintViolationException e) {
        log.error("onValidationException", e);
        Optional<ConstraintViolation<?>> first = e.getConstraintViolations().stream().findFirst();
        return first.map(constraintViolation -> new ResponseEntity<>(constraintViolation.getMessage(), HttpStatus.BAD_REQUEST)).orElse(null);
    }

    @ExceptionHandler(NullPointerException.class)
    ResponseEntity<String> onNotFoundException(NullPointerException e) {
        log.error("onNotFoundException", e);
        return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
    }

    private void buySell(TradeSide side, TradeRequest request, HttpServletResponse response) {
        log.debug("{} - request :: data={}", side, request);
        Trade trade = tradeService.create(side, request.getSymbol(), request.getQuantity(), request.getPrice());
        String location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/trades/" + trade.getUid()).toUriString();
        log.debug("{} - response :: trade={}, location={}", side, trade, location);
        response.setHeader(HttpHeaders.LOCATION, location);
        response.setStatus(HttpStatus.CREATED.value());
    }

}
