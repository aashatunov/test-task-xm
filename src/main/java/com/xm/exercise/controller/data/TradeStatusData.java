package com.xm.exercise.controller.data;

import com.xm.exercise.domain.TradeStatus;
import lombok.Data;

@Data
public class TradeStatusData {

    private TradeStatus status;

}
