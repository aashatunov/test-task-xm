package com.xm.exercise.controller.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TradeRequest {

    private String symbol;

    private Long quantity;

    private BigDecimal price;

}
