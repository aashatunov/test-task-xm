package com.xm.exercise.controller.data;

import com.xm.exercise.domain.TradeSide;
import com.xm.exercise.domain.TradeStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TradeData {

    private String id;
    private Long quantity;
    private String symbol;
    private TradeSide side;
    private BigDecimal price;
    private TradeStatus status;
    private String reason;
    private Date timestamp;

}
