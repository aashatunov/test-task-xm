package com.xm.exercise.domain;

public enum TradeSide {

    BUY,
    SELL

}
