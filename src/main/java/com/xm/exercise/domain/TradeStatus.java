package com.xm.exercise.domain;

public enum TradeStatus {

    NOT_EXECUTED,
    PENDING_EXECUTION,
    EXECUTED,

}
