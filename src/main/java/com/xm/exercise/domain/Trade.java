package com.xm.exercise.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Table
@Entity
@EqualsAndHashCode(of = {"id"})
public class Trade {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String uid;

    @Column(nullable = false)
    private String symbol;

    @Column(nullable = false, columnDefinition = "VARCHAR(255)")
    private BigDecimal price;

    @Column(nullable = false)
    private Long quantity;

    @Column(nullable = false)
    private Date createDate;

    @Column
    private Date updateDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TradeSide side;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TradeStatus status;

    @Column
    private String reason;

}
