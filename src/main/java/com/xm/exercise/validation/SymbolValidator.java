package com.xm.exercise.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class SymbolValidator implements ConstraintValidator<Symbol, String> {

    public static final String USD_JPY = "USD/JPY";
    public static final String EUR_USD = "EUR/USD";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && (value.equals(USD_JPY) || value.equals(EUR_USD));
    }

}
